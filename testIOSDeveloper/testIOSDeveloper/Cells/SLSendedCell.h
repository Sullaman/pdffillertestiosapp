
#import <UIKit/UIKit.h>

@interface SLSendedCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@property (retain, nonatomic) IBOutlet UILabel *sendStatusLabel;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@end
