
#import <UIKit/UIKit.h>

@interface SLReceivedCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;

@end
