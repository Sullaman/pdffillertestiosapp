
#import "SLViewController.h"
#import "SLSocketManager.h"
#import "SLSocketStatusViewController.h"
#import "SLSendedInfoViewController.h"
#import "SLReceivedInfoViewController.h"

@interface SLViewController ()

@property (retain, nonatomic) SLSocketStatusViewController *statusController;
@property (retain, nonatomic) SLSendedInfoViewController *sendedController;
@property (retain, nonatomic) SLReceivedInfoViewController *receivedController;

@property (retain, nonatomic) IBOutlet UIView *topView;
@property (retain, nonatomic) IBOutlet UIView *leftBottomView;
@property (retain, nonatomic) IBOutlet UIView *rightBottomView;

@end

@implementation SLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if (APP_DELEGATE.isInternetConnected)
    {
        [SOCKET_MANAGER connect];
    }
    
    self.statusController = (SLSocketStatusViewController *)[self addChildController:kStoryboardStatusControllerID
                                                                              toView:self.topView];
    self.sendedController = (SLSendedInfoViewController *)[self addChildController:kStoryboardSendedControllerID
                                                                            toView:self.leftBottomView];
    self.receivedController = (SLReceivedInfoViewController *)[self addChildController:kStoryboardReceivedControllerID
                                                                                toView:self.rightBottomView];
}

- (BOOL)shouldAutorotate
{
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

- (void)tapOnView:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

- (UIViewController *)addChildController:(NSString *)controllerID toView:(UIView *)theView
{
    UIViewController *theController = [self.storyboard instantiateViewControllerWithIdentifier:controllerID];
    [self addChildViewController:theController];
    [theView addSubview:theController.view];
    [theController didMoveToParentViewController:self];
    
    theController.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:theController.view
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:theView
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1
                                                                      constant:0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:theController.view
                                                                        attribute:NSLayoutAttributeBottom
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:theView
                                                                        attribute:NSLayoutAttributeBottom
                                                                       multiplier:1
                                                                         constant:0];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:theController.view
                                                                      attribute:NSLayoutAttributeLeft
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:theView
                                                                      attribute:NSLayoutAttributeLeft
                                                                     multiplier:1
                                                                       constant:0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:theController.view
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:theView
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1
                                                                        constant:0];
    
    [theView addConstraints:@[topConstraint, bottomConstraint, leftConstraint, rightConstraint]];
    [theView layoutIfNeeded];
    
    [theController.view addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(tapOnView:)] autorelease]];
    
    theController.view.layer.borderWidth = 0.5;
    theController.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    return theController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_statusController release];
    [_sendedController release];
    [_receivedController release];
    [_topView release];
    [_leftBottomView release];
    [_rightBottomView release];
    [super dealloc];
}
@end
