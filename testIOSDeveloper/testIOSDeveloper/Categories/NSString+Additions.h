
#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (BOOL)isXML;

@end
