
#import "NSString+Additions.h"

@interface NSString () <NSXMLParserDelegate>

@end

@implementation NSString (Additions)

- (BOOL)isXML
{
    NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:[self dataUsingEncoding:NSUTF8StringEncoding]] autorelease];
    parser.delegate = self;
    
    return [parser parse];
}

@end
