
#import <Foundation/Foundation.h>

@interface SLDataBase : NSObject

@property (retain, nonatomic) NSManagedObjectContext *context;
@property (retain, nonatomic) NSManagedObjectModel *model;
@property (retain, nonatomic) NSPersistentStoreCoordinator *storeCoordinator;

+ (instancetype)dataBase;

- (void)createNewMessage:(NSString *)message boolValue:(BOOL)boolValue inFormat:(FormatType)format;
- (void)addReceivedMessageID:(NSString *)messageID date:(NSDate *)date;

- (NSArray *)allMessages; //Sended and prepared
- (NSArray *)preparedForSending; //Not sended yet
- (NSArray *)receivedMessages; //Received

- (void)checkAndSendPreparedMessages;

@end
