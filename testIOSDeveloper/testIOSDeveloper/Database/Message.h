
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, retain) NSNumber * isEnable;
@property (nonatomic, retain) NSNumber * dataFormat;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * isReceived;
@property (nonatomic, retain) NSString * textMessage;

@end
