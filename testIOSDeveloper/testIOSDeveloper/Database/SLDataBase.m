
#import "SLDataBase.h"
#import <CoreData/CoreData.h>
#import "Message+Additions.h"
#import "SLSocketManager.h"
#import <XMLDictionary/XMLDictionary.h>

@interface SLDataBase ()

- (void)configure;

@end

@implementation SLDataBase

+ (instancetype)dataBase
{
    static id manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        manager = [self new];
        [manager configure];
    });
    
    return manager;
}

- (void)configure
{
    self.model = [NSManagedObjectModel mergedModelFromBundles:@[[NSBundle mainBundle]]];
    
    if (!self.model)
    {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
        self.model = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] autorelease];
    }
    
    self.storeCoordinator = [[[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.model] autorelease];
    [self.storeCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                        configuration:nil
                                                  URL:[self applicationStorePath]
                                              options:nil
                                                error:nil];
    
    self.context = [[[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType] autorelease];
    [self.context setPersistentStoreCoordinator:self.storeCoordinator];
}

#pragma mark - Messages

- (void)checkAndSendPreparedMessages
{
    NSArray *notSendedMessages = [[self preparedForSending] retain];
    
    if ([notSendedMessages count] && APP_DELEGATE.isInternetConnected)
    {
        for (Message *message in notSendedMessages)
        {
            [self sendMessage:message];
        }
    }
    
    [notSendedMessages release];
}

- (void)sendMessage:(Message *)message
{
    id dataForSending = nil;
    
    NSURL *instanceURL = message.objectID.URIRepresentation;
    NSURL *classURL = [instanceURL URLByDeletingLastPathComponent];
    NSString *classString = [classURL absoluteString];
    NSString *instanceId = [instanceURL lastPathComponent];
    
    [[NSUserDefaults standardUserDefaults] setObject:classString forKey:kDefaultsMessageClassURI];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *messageDict = @{kEventDateKey: [NSString stringWithFormat:@"%@", message.date],
                                  kEventMessageKey: message.textMessage,
                                  kEventMessageURIKey: instanceId,
                                  kEventBoolKey: message.isEnable};
    
    switch (message.dataFormat.integerValue)
    {
        case FormatTypeXML:
        {
            dataForSending = [messageDict XMLString];
            
            break;
        }
        case FormatTypeJSON:
        {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:messageDict
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:nil];
            
            dataForSending = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
            
            break;
        }
        case FormatTypeBinary:
        {
            NSError *jsonError = nil;
            
            dataForSending = [NSJSONSerialization dataWithJSONObject:messageDict
                                                             options:NSJSONWritingPrettyPrinted
                                                               error:&jsonError];
            
            
            break;
        }
    }
    
    [SOCKET_MANAGER send:dataForSending];
}

- (void)createNewMessage:(NSString *)message boolValue:(BOOL)boolValue inFormat:(FormatType)format
{
    Message *sendingObj = [Message addCreatedMessage:message
                                           boolValue:boolValue
                                                date:[NSDate date]
                                              format:format];
    
    if (sendingObj)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCreatedMessage
                                                            object:nil
                                                          userInfo:nil];
    }
    
    [self sendMessage:sendingObj];
}

- (void)addReceivedMessageID:(NSString *)messageID date:(NSDate *)date
{
    Message *receivedMessage = [[Message receivedMessage:messageID] retain];
    
    if (receivedMessage)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidSavedReceivedMessage
                                                            object:nil
                                                          userInfo:nil];
    }
    
    [receivedMessage release];
}

- (NSArray *)preparedForSending
{
    return [Message preparedMessages];
}

- (NSArray *)allMessages
{
    return [Message allMessages];
}

- (NSArray *)receivedMessages
{
    return [Message getReceivedMessages];
}

#pragma mark - PATHS

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}

- (NSURL *)applicationStorePath
{
    NSURL *storesDirectory = [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]] URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]])
    {
        NSError *error = nil;
        
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error])
        {
            NSLog(@"Successfully created Stores directory");
        }
        else
        {
            NSLog(@"FAILED to create directory: %@", error);
        }
    }
    
    return [storesDirectory URLByAppendingPathComponent:@"testIOS.sqlite"];
}

@end
