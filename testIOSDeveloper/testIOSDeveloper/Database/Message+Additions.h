
#import "Message.h"

@interface Message (Additions)

+ (Message *)addCreatedMessage:(NSString *)message
                     boolValue:(BOOL)boolValue
                          date:(NSDate *)date
                        format:(FormatType)format;

+ (Message *)receivedMessage:(NSString *)messageURI;

+ (NSArray *)getReceivedMessages;
+ (NSArray *)allMessages;
+ (NSArray *)preparedMessages;

@end
