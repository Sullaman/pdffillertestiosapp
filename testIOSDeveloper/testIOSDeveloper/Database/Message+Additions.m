
#import "Message+Additions.h"
#import "SLDataBase.h"
#import "NSString+Additions.h"

@implementation Message (Additions)

+ (Message *)addCreatedMessage:(NSString *)message
                     boolValue:(BOOL)boolValue
                          date:(NSDate *)date
                        format:(FormatType)format
{
    Message *theMessage = [(Message *)[[NSManagedObject alloc] initWithEntity:[NSEntityDescription entityForName:NSStringFromClass([self class])
                                                                                         inManagedObjectContext:[[SLDataBase dataBase] context]] insertIntoManagedObjectContext:[[SLDataBase dataBase] context]] autorelease];
    
    theMessage.dataFormat = [NSNumber numberWithInteger:format];
    theMessage.textMessage = message;
    theMessage.date = date;
    theMessage.isEnable = [NSNumber numberWithBool:boolValue];
    
    if ([DATA_BASE.context save:nil])
    {
        return theMessage;
    }
    else
    {
        return nil;
    }
}

+ (Message *)receivedMessage:(NSString *)messageURI
{
    NSString *classString = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsMessageClassURI];
    if (!classString)
    {
        return nil;
    }
    
    NSURL *reconstructedClassURL = [NSURL URLWithString:classString];
    NSURL *reconstructedInstanceURL = [reconstructedClassURL URLByAppendingPathComponent:messageURI];
    
    NSManagedObjectID *objectID = [DATA_BASE.storeCoordinator managedObjectIDForURIRepresentation:reconstructedInstanceURL];
    Message *theMessage = (Message *)[DATA_BASE.context objectWithID:objectID];
    
    if (!theMessage)
    {
        return nil;
    }
    
    theMessage.isReceived = @YES;
    
    if ([DATA_BASE.context save:nil])
    {
        return theMessage;
    }
    else
    {
        return nil;
    }
}

+ (NSArray *)getReceivedMessages
{
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])] autorelease];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isReceived == YES"];
    fetchRequest.predicate = predicate;
    
    return [DATA_BASE.context executeFetchRequest:fetchRequest error:nil];
}

+ (NSArray *)allMessages
{
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])] autorelease];
    
    return [DATA_BASE.context executeFetchRequest:fetchRequest error:nil];
}

+ (NSArray *)preparedMessages
{
    NSFetchRequest *fetchRequest = [[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])] autorelease];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isReceived != YES"];
    fetchRequest.predicate = predicate;
    
    return [DATA_BASE.context executeFetchRequest:fetchRequest error:nil];
}

//- (NSDictionary *)messageContent
//{
//    switch (self.dataFormat.integerValue)
//    {
//        case FormatTypeXML:
//        {
//            return [NSDictionary dictionaryWithXMLString:self.stringMessage];
//        }
//        case FormatTypeJSON:
//        {
//            return [NSJSONSerialization JSONObjectWithData:[self.stringMessage dataUsingEncoding:NSUTF8StringEncoding]
//                                                   options:NSJSONReadingMutableContainers
//                                                     error:nil];
//        }
//        case FormatTypeBinary:
//        {
//            NSError *jsonError = nil;
//            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self.binaryData
//                                                                     options:NSJSONReadingMutableContainers
//                                                                       error:&jsonError];
//            
//            if (!jsonError)
//            {
//                return jsonDict;
//            }
//            else
//            {
//                return nil;
//            }
//        }
//    }
//    
//    return nil;
//}

@end
