
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, MessageStatus) {
    MessageStatusPrepare,
    MessageStatusSended,
};

typedef NS_ENUM(NSUInteger, FormatType) {
    FormatTypeXML,
    FormatTypeJSON,
    FormatTypeBinary,
};

extern NSString * const kBaseServerURL;

//Events
extern NSString * const kEventMessageURIKey;
extern NSString * const kEventMessageKey;
extern NSString * const kEventDateKey;
extern NSString * const kEventBoolKey;

//Storyboard
extern NSString * const kStoryboardStatusControllerID;
extern NSString * const kStoryboardSendedControllerID;
extern NSString * const kStoryboardReceivedControllerID;

//Notifications
extern NSString * const kNotificationSocketConnecting;
extern NSString * const kNotificationSocketDidConnected;
extern NSString * const kNotificationSocketDidDisconnected;
extern NSString * const kNotificationCreatedMessage;
extern NSString * const kNotificationSendedMessage;
extern NSString * const kNotificationDidReceivedMessage;
extern NSString * const kNotificationDidSavedReceivedMessage;

//Log
extern NSString * const kLogMessageSocketConnecting;
extern NSString * const kLogMessageSocketDidConnected;
extern NSString * const kLogMessageSocketDidDisconnected;
extern NSString * const kLogMessageCreatedMessage;
extern NSString * const kLogMessageReceivedMessage;

extern NSString * const kLogMessageContentKey;
extern NSString * const kLogDateKey;

//Defaults
extern NSString * const kDefaultsMessageClassURI;