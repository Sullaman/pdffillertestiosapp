
#import "Constants.h"

NSString * const kBaseServerURL = @"ws://echo.websocket.org"; //http://www.websocket.org/echo.html

//Events
NSString * const kEventMessageURIKey = @"uri";
NSString * const kEventMessageKey = @"msg";
NSString * const kEventDateKey = @"date";
NSString * const kEventBoolKey = @"bool";

//Storyboard
NSString * const kStoryboardStatusControllerID = @"statusScreen";
NSString * const kStoryboardSendedControllerID = @"sendedScreen";
NSString * const kStoryboardReceivedControllerID = @"receivedScreen";

//Notifications
NSString * const kNotificationSocketConnecting = @"kNotificationSocketConnecting";
NSString * const kNotificationSocketDidConnected = @"kNotificationSocketDidConnected";
NSString * const kNotificationSocketDidDisconnected = @"kNotificationSocketDidDisconnected";
NSString * const kNotificationCreatedMessage = @"kNotificationCreatedMessage";
NSString * const kNotificationSendedMessage = @"kNotificationSendedMessage";
NSString * const kNotificationDidReceivedMessage = @"kNotificationDidReceivedMessage";
NSString * const kNotificationDidSavedReceivedMessage = @"kNotificationDidSavedReceivedMessage";

//Log
NSString * const kLogMessageSocketConnecting = @"Connecting";
NSString * const kLogMessageSocketDidConnected = @"Connected";
NSString * const kLogMessageSocketDidDisconnected = @"Disconnected";
NSString * const kLogMessageCreatedMessage = @"Sended";
NSString * const kLogMessageReceivedMessage = @"Received";

NSString * const kLogMessageContentKey = @"kLogMessageContentKey";
NSString * const kLogDateKey = @"kLogDateKey";

//Defaults
NSString * const kDefaultsMessageClassURI = @"kDefaultsMessageClassURI";