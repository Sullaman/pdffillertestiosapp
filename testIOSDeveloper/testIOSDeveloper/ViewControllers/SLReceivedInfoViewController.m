
#import "SLReceivedInfoViewController.h"
#import "SLReceivedCell.h"
#import "Message+Additions.h"

@interface SLReceivedInfoViewController () <UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SLReceivedInfoViewController
{
    NSArray *messages;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationDidSavedReceivedMessage
                                               object:nil];
    
    self.tableView.allowsSelection = NO;
    
    [self fetchMessages];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([messages count])
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[messages count] - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionNone
                                      animated:YES];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [messages release];
    [_tableView release];
    [super dealloc];
}

#pragma mark - Notification

- (void)didReceiveNotification:(NSNotification *)notification
{
    [self fetchMessages];
}

- (void)fetchMessages
{
    messages = [[DATA_BASE receivedMessages] retain];
    
    [self.tableView reloadData];
    
    if ([messages count])
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[messages count] - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionNone
                                      animated:YES];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [messages count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 0;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLReceivedCell *theCell = [tableView dequeueReusableCellWithIdentifier:@"receivedCell"];
    
    Message *currentMessage = messages[indexPath.row];
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    formatter.dateFormat = @" YYYY MMM dd HH:mm";
    
    theCell.messageLabel.text = currentMessage.textMessage;
    theCell.dateLabel.text = [formatter stringFromDate:currentMessage.date];
    
    return theCell;
}

@end
