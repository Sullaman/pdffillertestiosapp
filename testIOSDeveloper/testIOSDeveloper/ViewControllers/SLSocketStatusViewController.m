
#import "SLSocketStatusViewController.h"
#import "SLSocketManager.h"

@interface SLSocketStatusViewController ()

@property (retain, nonatomic) NSMutableString *logString;
@property (retain, nonatomic) IBOutlet UITextView *logTextView;
@property (retain, nonatomic) IBOutlet UIButton *sendButton;
@property (retain, nonatomic) IBOutlet UITextField *sendMessageTextField;
@property (retain, nonatomic) IBOutlet UISegmentedControl *dataFormatSegment;
@property (retain, nonatomic) IBOutlet UISwitch *theSwitch;

@property (retain, nonatomic) IBOutlet NSLayoutConstraint *textFieldRightConstraint;

@end

@implementation SLSocketStatusViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.logTextView addGestureRecognizer:[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTextView:)] autorelease]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationSocketDidConnected
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationSocketDidDisconnected
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationSendedMessage
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationDidReceivedMessage
                                               object:nil];
    
    self.logString = [NSMutableString new];
    self.logTextView.text = self.logString;
}

- (void)didReceiveNotification:(NSNotification *)notification
{
    NSDictionary *dict = notification.object;
    NSDate *eventDate = dict[kLogDateKey] ? dict[kLogDateKey] : [NSDate date];
    NSString *message = dict[kLogMessageContentKey] ? [NSString stringWithFormat:@"->\n %@", dict[kLogMessageContentKey]] : @"";
    
    NSString *eventName = notification.name;
    
    [self.logString appendString:@"\n"];
    if ([notification.name isEqualToString:kNotificationSocketConnecting])
    {
        eventName = kLogMessageSocketConnecting;
    }
    else if ([notification.name isEqualToString:kNotificationSocketDidConnected])
    {
        eventName = kLogMessageSocketDidConnected;
    }
    else if ([notification.name isEqualToString:kNotificationSocketDidDisconnected])
    {
        eventName = kLogMessageSocketDidDisconnected;
    }
    else if ([notification.name isEqualToString:kNotificationSendedMessage])
    {
        eventName = kLogMessageCreatedMessage;
    }
    else if ([notification.name isEqualToString:kNotificationDidReceivedMessage])
    {
        eventName = kLogMessageReceivedMessage;
    }
    
    [self.logString appendString:[NSString stringWithFormat:@"%@: %@ %@", eventDate, eventName, message]];
    self.logTextView.text = self.logString;
    
    [UIView animateWithDuration:0.4 animations:^{
        
        [self.logTextView scrollRangeToVisible:NSMakeRange(self.logTextView.text.length, 0)];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)tapOnTextView:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

- (IBAction)sendPressed:(id)sender
{
    if (!APP_DELEGATE.isInternetConnected)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Connect to the internet!", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }

    NSString *messageForSending = [[self.sendMessageTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] autorelease];
    
    if (!messageForSending.length)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"Enter the message!", nil)
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"Ok", nil)
                                              otherButtonTitles:nil];
        
        [alert show];
        [alert release];
        
        self.sendMessageTextField.text = nil;
        
        return;
    }
    
    [DATA_BASE createNewMessage:messageForSending
                      boolValue:self.theSwitch.on
                       inFormat:self.dataFormatSegment.selectedSegmentIndex];
}

- (IBAction)segmentValueChanged:(UISegmentedControl *)sender
{
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_logString release];
    [_sendButton release];
    [_sendMessageTextField release];
    [_dataFormatSegment release];
    [_theSwitch release];
    [_textFieldRightConstraint release];
    [_logTextView release];
    [super dealloc];
}
@end
