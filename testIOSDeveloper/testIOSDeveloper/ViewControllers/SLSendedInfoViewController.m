
#import "SLSendedInfoViewController.h"
#import "SLSendedCell.h"
#import "Message+Additions.h"

@interface SLSendedInfoViewController () <UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SLSendedInfoViewController
{
    NSArray *messages;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.allowsSelection = NO;
    
    [self fetchMessages];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationCreatedMessage
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveNotification:)
                                                 name:kNotificationSendedMessage
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([messages count])
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[messages count] - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionNone
                                      animated:YES];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [messages release];
    [_tableView release];
    [super dealloc];
}

#pragma mark - Notification

- (void)didReceiveNotification:(NSNotification *)notification
{
    [self fetchMessages];
}

- (void)fetchMessages
{
    messages = [[DATA_BASE allMessages] retain];
    
    [self.tableView reloadData];
    
    if ([messages count])
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:[messages count] - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionNone
                                      animated:YES];
    }
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [messages count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 0;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SLSendedCell *theCell = [tableView dequeueReusableCellWithIdentifier:@"sendedCell"];
    
    Message *currentMessage = messages[indexPath.row];
    
    theCell.messageLabel.text = currentMessage.textMessage;
    
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    formatter.dateFormat = @" YYYY MMM dd HH:mm";
    
    theCell.dateLabel.text = [formatter stringFromDate:currentMessage.date];
    theCell.sendStatusLabel.text = (currentMessage.isReceived.boolValue == YES ?
                                             NSLocalizedString(@"Sended", nil) : NSLocalizedString(@"Sending", nil));
    
    return theCell;
}

@end
