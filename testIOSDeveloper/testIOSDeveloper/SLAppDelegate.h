
#import <UIKit/UIKit.h>
#import <Reachability/Reachability.h>

@interface SLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic) BOOL isInternetConnected;

@end
