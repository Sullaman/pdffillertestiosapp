
#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>

typedef void (^SLCompletion)();

@interface SLSocketManager : NSObject

@property (retain, nonatomic) SRWebSocket *theSocket;

+ (instancetype)sharedManager;

- (void)connect;
- (void)disconnect;
- (void)send:(id)messageObject;

@end
