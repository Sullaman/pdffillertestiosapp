
#import "SLSocketManager.h"
#import "NSString+Additions.h"
#import <XMLDictionary/XMLDictionary.h>

@interface SLSocketManager () <SRWebSocketDelegate, NSXMLParserDelegate>

@end

@implementation SLSocketManager

+ (instancetype)sharedManager
{
    static id sharedManaged = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedManaged = [self new];
    });
    
    return sharedManaged;
}

- (void)connect
{
    if (!self.theSocket)
    {
        self.theSocket = [[[SRWebSocket alloc] initWithURL:[NSURL URLWithString:kBaseServerURL]] autorelease];
        self.theSocket.delegate = self;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSocketConnecting
                                                        object:@{kLogDateKey: [NSDate date]}
                                                      userInfo:nil];
    
    [self.theSocket open];
}

- (void)disconnect
{
    [self.theSocket close];
}

- (void)send:(id)messageObject
{
    if (!messageObject)
    {
        return;
    }
    
    [self.theSocket send:messageObject];
}

#pragma mark - SocketRocket

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSendedMessage
                                                        object:@{          kLogDateKey: [NSDate date],
                                                                           kLogMessageContentKey: message}
                                                      userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDidReceivedMessage
                                                        object:@{          kLogDateKey: [NSDate date],
                                                                 kLogMessageContentKey: message}
                                                      userInfo:nil];
    
    if ([message isKindOfClass:[NSData class]])
    {
        NSError *jsonError = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:message
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
        
        if (!jsonError)
        {
            NSString *messageURI = jsonDict[kEventMessageURIKey];
            if (messageURI)
            {
                [DATA_BASE addReceivedMessageID:messageURI date:[NSDate date]];
            }
        }
    }
    else if([message isKindOfClass:[NSString class]])
    {
        if ([message isXML])
        {
            NSLog(@"XML");
            
            NSDictionary *dict = [NSDictionary dictionaryWithXMLString:message];
            NSString *messageURI = dict[kEventMessageURIKey];
           
            if (messageURI)
            {
                [DATA_BASE addReceivedMessageID:messageURI date:[NSDate date]];
            }
            
        }
        else
        {
            NSLog(@"JSON");
            
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[message dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:nil];
            
            NSString *messageURI = jsonDict[kEventMessageURIKey];
            if (messageURI)
            {
                [DATA_BASE addReceivedMessageID:messageURI date:[NSDate date]];
            }
        }
    }
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSocketDidConnected
                                                        object:@{          kLogDateKey: [NSDate date],
                                                                 kLogMessageContentKey: [NSString stringWithFormat:@"to %@", webSocket.url]}
                                                      userInfo:nil];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSocketDidDisconnected
                                                        object:@{kLogDateKey: [NSDate date]}
                                                      userInfo:nil];
}

@end
